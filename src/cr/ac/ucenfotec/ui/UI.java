package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.Factura;

public class UI {

    public static void main(String[] args) {
        // Ejemplo de Composición
        Factura factura1 = new Factura("123","Juanito",2,3,2022);
        Factura factura2 = new Factura("456","Maria",15,01,2022);

        factura1.agregarLinea("P1",2,"Camisetas",10000);
        factura1.agregarLinea("P2",1,"Pantalón",15000);
    }
}
