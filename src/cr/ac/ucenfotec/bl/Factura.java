package cr.ac.ucenfotec.bl;

import java.util.ArrayList;

public class Factura {

    private String numero;
    private String cliente;
    private Fecha fecha; // relación de composición simple
    private ArrayList<Linea> detalleFactura; // relación de composición multiple

    public Factura() {
    }

    public Factura(String numero, String cliente, int dia, int mes, int anio) {
        this.numero = numero;
        this.cliente = cliente;
        this.fecha = new Fecha(dia,mes, anio); // Composición con la clase Fecha
        this.detalleFactura = new ArrayList<>();
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public void agregarLinea(String codigo, int cantidad, String descripcion, double precio){
        Linea linea = new Linea(codigo,cantidad,descripcion,precio);
        detalleFactura.add(linea);
    }
}
